# Marcopulse 2000

Please enjoy a [Demo of Marcopulse 2000](https://marcopulse-2000.fly.dev/).

## Rewriting a Single-Page-Application as Server-Side-Rendered Deno App using zero dependencies. But Why?

Recently I stumbled about an internship project of two junior developers.
The project consisted of a frontend javascript app and a corresponding Node.js API backend.
It looked more or less like the [Demo of Marcopulse 2000](https://marcopulse-2000.fly.dev/).
To be fair their original design looked a bit better than my reimplementation but this is a minor detail, fixable by some CSS improvements and nicer icons. Let's concentrate on the logic of the app.

## What did (does) the App do?

It displays the status of the pipelines (called "builds") of one Gitlab project, which are fetched via the Gitlab API by the backend, cached in a MongoDB and rendered to the frontend, which renders the HTML.
Basically it displays 4 counts and 2 tables of failed and running builds.
You can filter the builds by name and switch to a second "tab" which displays the builds in a historical order. There was also the possibility to assign a user to a failed build, but this feature was unfinished and did not do much beside of displaying a textbox. 

## How much code is needed to do what the app does?

Please take a guess!
How much code is needed to implement the described functionality?
How many libraries, frameworks and development dependencies are needed to ease the implementation?
How many (source code) files would the project has? How many lines of code?
How big would be the resulting web app?

Follow your gut feeling!

Please continue when you have found your answer.
...

I started web development around 1997 and somehow my gut feeling about expected code
complexity is formed by this time.
Back then, I would have implemented the app within one 200-line Perl- or PHP-Script, which rendered the HTML on the server

- No libraries.
- No frameworks.
- No (development) dependencies beside of an installed language runtime, and its standard library.
- One server-side 200 line script returning a 10kb HTML page
- No JavaScript frontend at all
- No knowledge is needed beside of the programming language and HTML.

5-10 years later I would had added some CSS to the mix. And maybe a bit of JQuery.

But nowadays...

## Some Stats of the Original Code

* 161 files in the git repo (50 JS,  13 SVG, 8 Handlebar templates, 6 Sass, ...)
* 52 direct dependencies
* Frontend:
  * 2629 packages from 1090 contributors
  * 50791 vulnerabilities (50779 low, 1 moderate, 11 high) found by `npm audit` 
  * 753 MB dependencies requiring 1.3 GiB of disk space in 172407 files
  * 3.6 MB frontend-build (mainly JavaScript), loads 2.6 MB at first page load
* Backend:
  * 9 MB dependencies requiring 14 MB of disk space in 1645 files 

Fun fact: The dependencies of this project need more hard drive space than my computer in 1997 had at all.

### Frontend Source Code Stats (excluding test code)

```
────────────────────────────────────
Language                 Files Code 
────────────────────────────────────
JavaScript                  15  186 
Handlebars                   8  145 
Sass                         6  474 
CSS                          1    0 
HTML                         1   20 
────────────────────────────────────
Total                       31  825 
────────────────────────────────────
```

### Complete Lines of Code Count (excluding test code)

When we count only JavaScript and Handlebars as code, we get together with the backend (119 LOC Javascript in 6 files) a complete line count of **459 LOC in 29 files**.


## What makes me sad in modern web development

I'm completely aware, that similar source code and dependency counts are the new normal in modern web development since some years.
Clearly junior developers should learn the State-Of-The-Art technologies in modern web development.
But somehow it makes me sad, that new developers might get the impression, that all this complexity, all the hidden magic, is really needed to build a simple web app.
Since some time I have the theory, that client side javascript rendering (React, Angular, Ember,....)
has actually increased the accidental complexity and reduced the productivity in web development
for many use cases. Therefore, I decided to reimplement the app just for fun as a server-side script using my early 2000s programming style.

## Reimplement the app as a server-side script running in Deno

I have chosen [Deno](https://deno.land) over other options just because it's shiny and new.
Actually I like shiny and new things, when they increase my productivity and reduce the accidental complexity.
There is a certain sweet irony of writing a webapp in an 20 years old style using a shiny new runtime.
I set myself the following artificial restrictions:

- No libraries, No frameworks. No dependencies beside of an installed language runtime, and its standard library.
- No JavaScript frontend at all, server-side rendering
- No knowledge is needed beside of the programming language, HTML and a little of CSS.
- 1 server-side TypeScript file containing all app specific code
- 1 CSS File
- Some TypeScript files containing generic utility functions. Deno has a pretty minimal standard lib. Without using external libraries, I will have to write some generic utils and want them to be separated from the app specific code.

I contrast to the original code I used Redis as cache instead of MongoDB, because Redis is free at my web hoster. Actually I think just caching in a simple variable would have been sufficient, but to allow a fairer comparison with the original code I added this "database"-based caching. Considering my restrictions to not use any library, this meant rewriting a Redis database driver from scratch, which was pretty easy for my restricted use case.

Please have a look at the [live demo](https://marcopulse-2000.fly.dev/) 
and at the [source code](https://gitlab.com/shygroup/marcopulse-2000/).
All [app specific code is located in one source file](https://gitlab.com/shygroup/marcopulse-2000/-/blob/master/src/main.ts).

## Comparison with Original Code
<table>
<tr>
  <th></th>
  <th>Original Code SPA</th>
  <th>Server Side Reimplementation</th>
</tr>
<tr>
  <th>Frontend-Dependencies</th>
  <td>753 MB (1.3 GB Disk Space)</td>
  <td>0</td>
</tr>
<tr>
  <th>Backend-Dependencies</th>
  <td>9 MB</td>
  <td>0</td>
</tr>
<tr>
  <th>Data-Transfer at first Page-Load</th>
  <td>2600 KB (524KB compressed)</td>
  <td>7KB (5KB compressed)</td>
</tr>
<tr>
  <th>Code Files</th>
  <td>29</td>
  <td>4 = 1 app specific + 3 generic utils</td>
</tr>
<tr>
  <th>Lines of Code</th>
  <td>459</td>
  <td>265 = 176 app specific + 92 generic utils</td>
</tr>
</table>

As you can see, although I used zero libraries, the reimplementation has only half the lines of code. This especially remarkable considering that I have used a statically typed programming language (TypeScript) instead of plain JavaScript.

## Conclusions

Would I recommend my approach for professional development?

### Zero dependencies
Implementing a [database-driver](https://gitlab.com/shygroup/marcopulse-2000/-/blob/master/src/utils/redis.ts) from scratch
to avoid dependencies is surely rarely something I would recommend. 
Implementing your own [static file server](https://gitlab.com/shygroup/marcopulse-2000/-/blob/master/src/utils/serve-files.ts) is equally wasteful.
However, the general idea of questioning the need for dependencies and frameworks is a mind set, 
I can recommend in order to avoid the drawbacks of dependencies and frameworks, if not needed.

### Server-Side-Rendering instead of SPA
I'm not against SPAs. They have legitimate use cases. 
I'm only against considering them as the default approach. 
Considering the drawbacks of SPAs, you should think about the feasibility of a server-side approach first.


## How to Start Locally

Install Deno (https://deno.land/#installation).

```bash
deno run --allow-net --allow-env --allow-read src/main.ts
```

Run redis for example using docker:

```bash
docker run --name container-redis -p 6379:6379 -d redis
docker start container-redis
```

## Docker build

```bash
docker image build -t marcopulse-2000 .
docker run -p 8080:8080 marcopulse-2000
```

## Run inside docker locally using Docker Compose

Install Docker Compose (https://docs.docker.com/compose/install).

Run:
```
docker-compose up --build
```

## Deployment to fly.io

Install [Fly](https://fly.io/docs/getting-started/installing-flyctl/):

```bash
fly deploy
```

## License
 
MIT
 
## Copyright

Copyright (c) 2020 Marco Stahl 

