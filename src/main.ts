import {serve} from 'https://deno.land/std/http/server.ts';
import {RedisConnection} from './utils/redis.ts';
import {serveStaticFile} from './utils/serve-files.ts';
import {escapeHtml, timeAgo} from './utils/utils.ts';

const GITLAB_PROJECT_ID = '21230477';

enum PagePath {
  index = '/',
  history = '/history',
}

const QUERY_PARAM = 'query';

async function main() {
  console.log('Starting server at http://localhost:8080');
  pollBuildsFromGitlab();
  for await (const req of serve({port: 8080})) {
    try {
      const [urlPath, searchParamsString] = req.url.split('?');
      console.log('urlPath', urlPath);
      if (urlPath === PagePath.index || urlPath === PagePath.history) {
        req.respond({
          headers: new Headers({'Content-Type': 'text/html; charset=utf-8'}),
          body: await renderPage(urlPath, new URLSearchParams(searchParamsString))
        });
      } else {
        await serveStaticFile(req);
      }
    } catch (error) {
      if (error instanceof Deno.errors.NotFound) {
        req.respond({body: 'Not found', status: 404})
      } else {
        req.respond({body: error.message, status: 500})
      }
    }
  }
}

interface Build {
  name: string;
  status: 'running' | 'success' | 'failed';
  date: string;
  link: string;
}

const storage = await RedisConnection.create(Deno.env.get('FLY_REDIS_CACHE_URL'));
const STORAGE_KEY_BUILDS = 'builds';

async function pollBuildsFromGitlab() {
  try {
    const project = await fetch('https://gitlab.com/api/v4/projects/' + GITLAB_PROJECT_ID).then(r => r.json());
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const pipelines: any[] = await fetch(`https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/pipelines`).then(r => r.json());

    const builds: Build[] = pipelines.map(pipeline => ({
      name: `${project.name} (ID: ${pipeline.id})`,
      status: pipeline.status.toLowerCase(),
      date: pipeline.created_at,
      link: pipeline.web_url,
    }));

    storage.set(STORAGE_KEY_BUILDS, builds);
  } catch (error) {
    console.error('Error while polling builds from Gitlab', error);
  }

  setTimeout(pollBuildsFromGitlab, 10_000); // recursive loop
}

export async function renderPage(path: PagePath, searchParams: URLSearchParams) {
  const builds = (await storage.get<Build[]>(STORAGE_KEY_BUILDS)) ?? [];

  const runningBuilds = builds.filter(it => it.status === 'running');
  const failedBuilds = builds.filter(it => it.status === 'failed');
  const successfulBuilds = builds.filter(it => it.status === 'success');
  const hasLatestBuildFailed = builds[0]?.status === 'failed';

  const query = searchParams.get(QUERY_PARAM)?.toLowerCase();
  const filterByQuery = (builds: Build[]) => query
    ? builds.filter(it => it.name.toLowerCase().includes(query))
    : builds;

  const tables = (path === PagePath.history)
    ? renderHistoryTable(filterByQuery(builds))
    : renderDashboardTables(filterByQuery(failedBuilds), filterByQuery(runningBuilds));

  return `<!DOCTYPE html>
    <html lang="en">
      <head>
        <title>Marcopulse 2000</title>
        <link rel="stylesheet" href="style.css">
      </head>
    <body>
      <header>
        <h1>Marcopulse 2000</h1>
      </header>
      <section id="stats" class="${hasLatestBuildFailed ? 'failed' : 'successful'}">
        <div>
          <div class="labeled-number total-builds">Total Builds: <strong>${builds.length}</strong></div>    
          <div class="labeled-number running">Running: <strong>${runningBuilds.length}</strong></div>    
        </div>
        <figure id="current-status">
          ${renderCurrentStatus(hasLatestBuildFailed)}        
          <figcaption>Current Status</figcaption> 
        </figure>
        <div>
          <div class="labeled-number successful">Successful: <strong>${successfulBuilds.length}</strong></div>    
          <div class="labeled-number failed">Failed: <strong>${failedBuilds.length}</strong></div>    
        </div>
      </section>
      
      <main>
        <header>
          <form id="search" action="" method="get">
            <input type="search" name="${QUERY_PARAM}" aria-label="Search builds" autofocus
              placeholder="Search for builds by name" 
              value="${escapeHtml(query ?? '')}">
            <button>Search</button>
          </form>
          <nav>
            ${link(PagePath.index, 'index-tab-icon', path, 'Dashboard', query)}
            ${link(PagePath.history, 'history-tab-icon', path, 'History', query)}
          </nav>
        </header>
        
        ${tables}
        
      </main>
    </body>
    </html>
  `;
}

function link(path: PagePath, cssClass: string, currentPath: string, tooltip: string, query?: string) {
  const ariaCurrent = path === currentPath ? 'page' : 'false'
  const href = path + (query ? (`?${QUERY_PARAM}=${encodeURIComponent(query)}`) : '');
  return `<a href="${href}" class="${cssClass}" aria-current=${ariaCurrent} title="${tooltip}"></a>`;
}

function renderDashboardTables(failedBuilds: Build[], successfulBuilds: Build[]) {
  return `
    <div class="dashboard-tables">
      <section id="failed-table" ">
        <h2>Failed Builds</h2>
        ${renderBuildsTable(failedBuilds)}
      </section>
    
      <section id="running-table">
        <h2>Running Builds</h2>
        ${renderBuildsTable(successfulBuilds)}
      </section>
    </div>
  `;
}

function renderHistoryTable(builds: Build[]) {
  return `
    <section id="history-table">
        ${renderBuildsTable(builds, true)}
    </section>
  `;
}

function renderCurrentStatus(hastLatestBuildFailed: boolean) {
  return hastLatestBuildFailed
    ? `<img src="/images/failed.svg" alt="Red X" title="Latest build has failed."/>`
    : `<img src="/images/success.svg" alt="Green Checkmark" title="Latest build is fine."/>`;
}

function renderBuildsTable(builds: Build[], displayStatusColumn = false) {
  const STATUS_COLUMN = {
    failed: '<img src="/images/failed.svg" alt="Failed" title="Failed"/>',
    success: '<img src="/images/success.svg" alt="Success" title="Successful"/>',
    running: '<img src="/images/running.svg" alt="Running" title="Still running"/>',
  };

  return `
    <table>
      <thead>
        <tr>
          ${displayStatusColumn ? `<th class="status-column">Status</th>` : ''}
          <th>Name</th>
          <th class="date-column">Date</th>
          <th class="claimed-by-column">Claimed by</th>
        </tr>
      </thead>
      <tbody>
        ${builds.map(build => `
          <tr>
            ${displayStatusColumn ? `<td class="status-column">${STATUS_COLUMN[build.status]}</td>` : ''}
            <th><a href="${build.link}">${build.name}</a></th>
            <td class="date-column" title="${build.date}">${timeAgo(new Date(build.date))}</td>
            <td class="claimed-by-column"><button>Claim it</button></td>
          </tr>   
        `).join('')}
      </tbody>
    </table> 
  `;
}

main();
