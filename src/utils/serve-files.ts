import {serveFile} from 'https://deno.land/std/http/file_server.ts';
import {ServerRequest} from 'https://deno.land/std/http/server.ts';
import {posix} from 'https://deno.land/std/path/mod.ts';

export async function serveStaticFile(req: ServerRequest) {
  const urlPath = req.url.replace(/\?.+/, '');
  const filePath = posix.join('static', decodeURIComponent(posix.normalize(urlPath)));
  const response = await serveFile(req, filePath);
  if (filePath.endsWith('.svg')) {
    response.headers!.set('content-type', 'image/svg+xml')
  }
  req.respond(response);
}
