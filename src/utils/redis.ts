import {BufReader, BufWriter} from 'https://deno.land/std@0.70.0/io/bufio.ts';

// https://redis.io/topics/protocol
// https://www.redisgreen.net/blog/beginners-guide-to-redis-protocol/
// https://lethain.com/redis-protocol/

const encoder = new TextEncoder();
const decoder = new TextDecoder();

export class RedisConnection {
  private constructor(private readonly conn: Deno.Conn, private readonly reader: BufReader, private readonly writer: BufWriter) {
  }

  static async create(redisUrl = 'redis://localhost:6379') {
    const urlParts = redisUrl.match(/redis:\/\/(:(?<password>.*)@)?(?<hostname>.*)(:(?<port>\d+)).*/)?.groups;

    if (!urlParts) {
      throw new Error(`Invalid RedisUrl "${redisUrl}"`);
    }

    const conn = await Deno.connect({hostname: urlParts['hostname'], port: parseInt(urlParts['port'])});
    const redisConnection = new RedisConnection(conn, new BufReader(conn), new BufWriter(conn));

    if (urlParts['password']) {
      await redisConnection.sendCommand('AUTH', urlParts['password']);
      await redisConnection.readLine();
    }

    return redisConnection;
  }

  async set(key: string, value: unknown) {
    await this.sendCommand('SET', key, JSON.stringify(value));
    await this.readLine(); // hopefully the OK response
  }

  async get<T>(key: string): Promise<T | undefined> {
    await this.sendCommand('GET', key);

    const lengthString = await this.readLine();
    if (lengthString === '$-1') {
      return undefined;
    }

    const jsonString = await this.readLine();
    if (!jsonString) {
      console.error(`Redis: Could not read line. lengthString="${lengthString}"`);
      return undefined;
    }

    return JSON.parse(jsonString);
  }

  private async sendCommand(command: string, ...args: string[]) {
    await this.writer.write(encoder.encode(createCommandString(command, ...args)));
    await this.writer.flush();
  }

  private async readLine() {
    const lineResult = await this.reader.readLine();
    if (!lineResult) {
      return undefined;
    }
    return decoder.decode(lineResult.line);
  }
}

function createCommandString(command: string, ...args: string[]) {
  const allParts = [command, ...args];
  const lines = allParts.flatMap(it => ['$' + encoder.encode(it).byteLength, it]);
  return ['*' + allParts.length, ...lines].join('\r\n') + '\r\n';
}
