export function escapeHtml(unsafe: string) {
  return unsafe
    .replace(/&/g, "&amp;")
    .replace(/</g, "&lt;")
    .replace(/>/g, "&gt;")
    .replace(/"/g, "&quot;")
    .replace(/'/g, "&#039;");
}

function formatUnitAgo(value: number, unit: string) {
  const intValue = Math.floor(value);
  return intValue + ' ' + unit + (intValue === 1 ? '' : 's') + ' ago';
}

export function timeAgo(date: Date, now = Date.now()) {
  const pastSeconds = (now - date.getTime()) / 1000;
  if (pastSeconds < 60) {
    return 'just now';
  } else if (pastSeconds < 60 * 60) {
    return formatUnitAgo(pastSeconds / 60, 'minute');
  } else if (pastSeconds < 60 * 60 * 24) {
    return formatUnitAgo(pastSeconds / 60 / 60, 'hour');
  } else {
    return formatUnitAgo(pastSeconds / 60 / 60 / 24, 'day');
  }
}
