import {assertEquals} from 'https://deno.land/std@0.68.0/testing/asserts.ts';
import {timeAgo} from '../src/utils/utils.ts';

const now = 1_000_000_000;

function assertTimeAgo(pastSeconds: number, expectedString: string) {
  assertEquals(timeAgo(new Date(now - pastSeconds * 1000), now), expectedString);
}

Deno.test('timeAgo - under one minute => just now', () => {
  assertTimeAgo(0, 'just now');
  assertTimeAgo(59, 'just now');
});

Deno.test('timeAgo - under one hour => minute(s)', () => {
  assertTimeAgo(60, '1 minute ago');
  assertTimeAgo(61, '1 minute ago');
  assertTimeAgo(120, '2 minutes ago');
  assertTimeAgo(60 * 60 - 1, '59 minutes ago');
});

Deno.test('timeAgo - under one day => hour(s)', () => {
  assertTimeAgo(60 * 60, '1 hour ago');
  assertTimeAgo(2 * 60 * 60 - 1, '1 hour ago');
  assertTimeAgo(2 * 60 * 60, '2 hours ago');
  assertTimeAgo(24 * 60 * 60 - 1, '23 hours ago');
});

Deno.test('timeAgo - more than one day => day(s)', () => {
  assertTimeAgo(60 * 60 * 24, '1 day ago');
  assertTimeAgo(2 * 60 * 60 * 24, '2 days ago');
});
