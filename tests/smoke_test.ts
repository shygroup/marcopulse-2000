import {assertStringContains} from 'https://deno.land/std@0.68.0/testing/asserts.ts';

Deno.test('Smoke test of an already running server', async () => {
  const html = await fetch('http://localhost:8080').then(it => it.text());
  assertStringContains(html, 'Marcopulse 2000');
});

